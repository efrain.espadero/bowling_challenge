﻿
namespace Bowling.WebAPI.Controllers
{
    using Bowling.Application.Interfaces;
    using Bowling.Domain.Entities;
    using Microsoft.AspNetCore.Mvc;

    [Route("api/[controller]")]
    [ApiController]
    public class ScoresController : ControllerBase
    {
        private readonly IScoreService _scoreService;
        public ScoresController(IScoreService scoreService)
        {
            _scoreService = scoreService;
        }
        [HttpGet]
        public async Task<List<Score>> GetAlls()
        {
            return await _scoreService.GetScoresAsync();
        }

        [HttpPost]
        public async Task<Score> CreateScore([FromBody] Score model)
        {
            return await _scoreService.CreateScoreAsync(model);
        }

        [HttpGet("points/{id}")]
        public async Task<IActionResult> GetTotalPoints(string id)
        {
            int points = await _scoreService.GetTotalPoints(id);
            return Ok(new { result = "Total Puntuation", points = points });
        }

    }
}
