﻿
namespace Bowling.WebAPI.Controllers
{
    using Bowling.Application.Interfaces;
    using Bowling.Domain.Entities;
    using Microsoft.AspNetCore.Mvc;

    [Route("api/[controller]")]
    [ApiController]
    public class GameController : ControllerBase
    {
        private readonly IGameService _gameService;
        public GameController(IGameService gameService)
        {
            this._gameService = gameService;

        }

        [HttpGet]
        public async Task<List<Game>> GetGameAlls()
        {
            return await _gameService.GetGamesAsync();
        }
        
        [HttpPost]
        public async Task<Game> CreateGame([FromBody]Game data)
        {
            return await _gameService.CreateGameAsync(data);
        }


    }
}
