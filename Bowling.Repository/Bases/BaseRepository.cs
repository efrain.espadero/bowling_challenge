﻿
namespace Bowling.Repository.Bases
{
    using Bowling.Repository.Configuration;
    public abstract class BaseRepository
    {
        private readonly DatabaseConfiguration _databaseConfiguration;
        public BaseRepository(DatabaseConfiguration databaseconfiguration)
        {
            this._databaseConfiguration = databaseconfiguration;
        }
    }
}
