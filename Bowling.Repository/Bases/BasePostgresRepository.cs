﻿
namespace Bowling.Repository.Bases
{
    using Bowling.Repository.Configuration;
    using Npgsql;
    using System.Data;
    public class BasePostgresRepository : BaseRepository
    {
        private readonly PostgresDBConfiguration _postgresDBConfiguration;

        public BasePostgresRepository(DatabaseConfiguration databaseconfiguration) : base(databaseconfiguration)
        {
            this._postgresDBConfiguration = databaseconfiguration.Postgres;
          //  dbSchema = databaseconfiguration.DatabaseName;
        }

        public IDbConnection GetConnection()
        {
            return new NpgsqlConnection(_postgresDBConfiguration.ConnectionString);
        }

    }
}
