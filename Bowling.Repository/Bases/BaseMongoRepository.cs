﻿
namespace Bowling.Repository.Bases
{
    using Bowling.Repository.Configuration;
    using MongoDB.Driver;
    public class BaseMongoRepository : BaseRepository
    {
        private readonly MongoDBConfiguration _mongoDbConfiguration;
        protected IMongoDatabase store;
        protected string collectionName { get; }
        public BaseMongoRepository(DatabaseConfiguration databaseConfiguration, string collectionName) : base(databaseConfiguration)
        {
            this.collectionName = collectionName;
            this._mongoDbConfiguration = databaseConfiguration.MongoDb;
            this.store = new MongoClient(_mongoDbConfiguration.ConnectionString).GetDatabase(databaseConfiguration.DatabaseName);
        }
    }
}
