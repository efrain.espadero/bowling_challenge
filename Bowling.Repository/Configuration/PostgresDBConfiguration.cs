﻿
namespace Bowling.Repository.Configuration
{
    public class PostgresDBConfiguration
    {
        public const string Section = "Postgres";
        public string ConnectionString  { get; set; }
    }
}
