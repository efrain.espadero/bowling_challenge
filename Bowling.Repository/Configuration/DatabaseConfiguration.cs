﻿
namespace Bowling.Repository.Configuration
{
    using Bowling.Repository.Types;
    public class DatabaseConfiguration
    {
        public string Section = "DatabaseConfiguration";
        public string DatabaseName { get; set; }
        public DataStoreType Database { get; set; }    
        public PostgresDBConfiguration Postgres { get; set; }
        public MongoDBConfiguration MongoDb { get; set; }



    }
}
