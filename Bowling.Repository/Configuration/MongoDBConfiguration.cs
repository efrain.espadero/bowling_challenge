﻿namespace Bowling.Repository.Configuration
{
    public class MongoDBConfiguration
    {
        public const string Section = "MongoDb";
        public string ConnectionString { get; set; }
    }
}
