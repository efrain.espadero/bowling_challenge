﻿namespace Bowling.Repository.Types
{
   public enum DataStoreType
    {
        Postgres,
        MongoDB
    }
}
