﻿
namespace Bowling.Repository.Services
{
    using Bowling.Domain.Entities;
    using Bowling.Domain.Interfaces;
    using Bowling.Repository.Configuration;
    using Bowling.Repository.Models;
    using Bowling.Repository.Types;
    using Microsoft.Extensions.Configuration;
    using Microsoft.Extensions.DependencyInjection;
    public class UnitOfWork : IUnitOfWork
    {
        private readonly IBaseRepositoryAsync<Game> _gameRepository;
        private readonly IBaseRepositoryAsync<Score> _scoreRepository;

        public UnitOfWork(IBaseRepositoryAsync<Game> gameRepository, IBaseRepositoryAsync<Score> scoreRepository)
        {
            _gameRepository = gameRepository;
            _scoreRepository = scoreRepository;
        }

        public static void ConfigureServices(IServiceCollection services, IConfiguration configuration)
        {
            services.AddTransient<IUnitOfWork, UnitOfWork>();

            DatabaseConfiguration databaseConfiguration = new DatabaseConfiguration();
            configuration.GetSection(databaseConfiguration.Section). Bind(databaseConfiguration);
            services.AddSingleton<DatabaseConfiguration>(databaseConfiguration);

            // data store
            
            DataStoreType dataProviderType = databaseConfiguration.Database;

            switch (dataProviderType)
            {
                case DataStoreType.MongoDB:
                     // mongodb
                    services.AddTransient<IBaseRepositoryAsync<string>, MongoRepository<string>>();
                    services.AddTransient<IBaseRepositoryAsync<Score>, MongoRepository<Score>>();
                    break;
                case DataStoreType.Postgres:
                    // postgres 
                    services.AddTransient<IBaseRepositoryAsync<Game>, PostgresRespository<Game>>();
                    services.AddTransient<IBaseRepositoryAsync<Score>, PostgresRespository<Score>>();
                    break;
                default:
                    // MSSQL
                    break;
            }
        }

        public IBaseRepositoryAsync<T> gameRepository<T>()
        {
            return (IBaseRepositoryAsync<T>)_gameRepository;
        }

        public IBaseRepositoryAsync<T> scoreRepository<T>()
        {
            return (IBaseRepositoryAsync<T>)_scoreRepository;
        }
    }
}
