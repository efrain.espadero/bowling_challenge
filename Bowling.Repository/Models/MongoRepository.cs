﻿
namespace Bowling.Repository.Models
{
    using Bowling.Domain.Interfaces;
    using Bowling.Repository.Bases;
    using Bowling.Repository.Configuration;
    using MongoDB.Bson;
    using MongoDB.Bson.Serialization;
    using MongoDB.Driver;
    public class MongoRepository<T> : BaseMongoRepository , IBaseRepositoryAsync<T>
    {
        private string GameCollection = typeof(T).Name.ToLower();
        public MongoRepository(DatabaseConfiguration databaseConfiguration): base(databaseConfiguration, typeof(T).Name.ToLower())
        {

        }
        public async Task<T> CreateAsync(T model)
        {
            try
            {

                IMongoCollection<BsonDocument> collection = this.store.GetCollection<BsonDocument>(GameCollection);
                var document = model.ToBsonDocument();
                document["_id"] = ObjectId.GenerateNewId();
                await collection.InsertOneAsync(document);
                return BsonSerializer.Deserialize<T>(document);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async Task DeleteByIdAsync(string id)
        {
            try
            {
                IMongoCollection<BsonDocument> collection =this.store.GetCollection<BsonDocument>(GameCollection);
                var delDocument = Builders<BsonDocument>.Filter.Eq("_id", ObjectId.Parse(id));
                await collection.DeleteOneAsync(delDocument);
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public Task<T> GetByIdAsync(string id)
        {
            throw new NotImplementedException();
        }

        public async Task<List<T>> GetAllsAsync()
        {
            try
            {

                IMongoCollection<BsonDocument> collection = this.store.GetCollection<BsonDocument>(GameCollection);
                var data = collection.Find(new BsonDocument()).ToList();
                var custs = new List<T>();
                data.ForEach(x => custs.Add(BsonSerializer.Deserialize<T>(x)));
                return custs;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public Task<T> UpdateAsync(T model, string id)
        {
            throw new NotImplementedException();
        }
    }
}
