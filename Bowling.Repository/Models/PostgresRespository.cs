﻿
namespace Bowling.Repository.Models
{
    using Bowling.Domain.Interfaces;
    using Bowling.Repository.Bases;
    using Bowling.Repository.Configuration;
    using Dapper;
    using System.Data;
    public class PostgresRespository<T> : BasePostgresRepository, IBaseRepositoryAsync<T>
    {
        public PostgresRespository(DatabaseConfiguration databaseconfiguration) : base(databaseconfiguration)
        {
        }

        public async Task<T> CreateAsync(T model)
        {

            try
            {
                using (IDbConnection conn = GetConnection())
                {
                    int val = await conn.ExecuteScalarAsync<int>(CreateInsertSqlQuery(model));
                    var result = await conn.QueryFirstOrDefaultAsync<T>($"SELECT * FROM public.{typeof(T).Name.ToLower()} WHERE {typeof(T).GetProperties()[0].Name.ToLower()}={val}");
                    return result;
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public Task DeleteByIdAsync(string id)
        {
            throw new NotImplementedException();
        }

        public async Task<List<T>> GetAllsAsync()
        {
            try
            {
                using (IDbConnection conn = GetConnection())
                {
                    conn.Open();
                    var querySQL = @"SELECT * FROM public." + typeof(T).Name.ToLower() + ";";
                    var data = (await conn.QueryAsync<T>(querySQL)).ToList();
                    return data;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public Task<T> GetByIdAsync(string id)
        {
            throw new NotImplementedException();
        }

        public Task<T> UpdateAsync(T model, string id)
        {
            throw new NotImplementedException();
        }

        public string CreateInsertSqlQuery(T model)
        {
            string initQuery = "INSERT INTO public." + typeof(T).Name.ToLower() + " (";
            string values = "(";
            for (int i = 1; i < typeof(T).GetProperties().Length; i++)
            {
                initQuery=initQuery + typeof(T).GetProperties()[i].Name.ToLower();
                if (typeof(T).GetProperties()[i].PropertyType == typeof(int))
                {
                    values = values + $"{typeof(T).GetProperties()[i].GetValue(model)}";
                }
                else if (typeof(T).GetProperties()[i].PropertyType == typeof(string))
                {
                    values = values + $"'{typeof(T).GetProperties()[i].GetValue(model)}'";
                }
                if (i < typeof(T).GetProperties().Length-1)
                {
                    values = values + ",";
                    initQuery = initQuery + ",";
                }

            }
            initQuery = initQuery +") VALUES " +values+$") RETURNING {typeof(T).GetProperties()[0].Name.ToLower()}";

            return initQuery;
        }
    }
}
