﻿
namespace Bowling.Domain.Interfaces
{
    using System.Collections.Generic;
    using System.Threading.Tasks;
    public interface IBaseRepositoryAsync<T> where T: Entities
    {
        Task<List<T>> GetAllsAsync();
        Task<T> GetByIdAsync(string id);
        Task DeleteByIdAsync(string id);
        Task<T> CreateAsync(T model);
        Task<T> UpdateAsync(T model, string id);



    }


}


var repo = new MongoRepository<string>();
var a = repo.GetGameByIdAsync("1");