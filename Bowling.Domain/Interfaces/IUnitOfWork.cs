﻿
namespace Bowling.Domain.Interfaces
{
    public interface IUnitOfWork
    {
        IBaseRepositoryAsync<T> gameRepository<T>();
        IBaseRepositoryAsync<T> scoreRepository<T>();
    }
}
