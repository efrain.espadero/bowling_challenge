﻿
namespace Bowling.Domain.Entities
{
    using Bowling.Domain.Interfaces;
    using MongoDB.Bson;
    using MongoDB.Bson.Serialization.Attributes;
    using System.ComponentModel.DataAnnotations;
    public class Game
    {
        public string? Id { get; set; }
     
        public string GameName { get; set; }

        public string Status { get; set; }

        public string PlayerName { get; set; }

        public IEnumerable<Score> Scores { get; set; }

        public string Difficulty { get; set; }
        
    }
}
