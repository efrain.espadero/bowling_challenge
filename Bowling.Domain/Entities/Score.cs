﻿
namespace Bowling.Domain.Entities
{
    using Bowling.Domain.Interfaces;
    using MongoDB.Bson;
    using MongoDB.Bson.Serialization.Attributes;
    public class Score
    {
        public string? Id{ get; set; }
        public int Point { get; set; }
        public string KnockedBowling { get; set; }
        public int Turn { get; set; }
        public string GameId { get; set; }
    }
}
