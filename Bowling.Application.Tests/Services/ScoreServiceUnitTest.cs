﻿
namespace Bowling.Application.Tests.Services
{
    using Bowling.Application.Interfaces;
    using Bowling.Domain.Entities;
    using Bowling.Domain.Interfaces;
    using Moq;
    using System.Collections.Generic;
    using System.Linq;
    using Xunit;

    public class ScoreServiceUnitTest
    {

        [Fact]
        public void Test_GetAllScoresAsync()
        {
            var scores = new List<Score>() { new Score { _id = "343242323" , Point=10, KnockedBowling="X", Turn=1, Game_id="123456"} };
            var scoreRepository = new Mock<IUnitOfWork>();
            var scoreService = new Mock<IScoreService>();
            scoreRepository.Setup(x => x.gameRepository<Score>().GetAllsAsync()).ReturnsAsync(scores.ToList());
            scoreService.Setup(x => x.GetScoresAsync()).ReturnsAsync(scores);
        }

        [Fact]
        public void Test_CreateScoreAsync()
        {
            Score data = new Score { _id = "343242323", Point = 10, KnockedBowling = "X", Turn = 1, Game_id = "123456" };
            var scoreRepository = new Mock<IUnitOfWork>();
            var scoreService = new Mock<IScoreService>();
            scoreRepository.Setup(x => x.scoreRepository<Score>().CreateAsync(data)).ReturnsAsync(data);
            scoreService.Setup(x => x.CreateScoreAsync(data)).ReturnsAsync(data);        
        }

        [Fact]
        public void Test_GetTotalPointsAsync()
        {
            var scores = new List<Score>() { new Score { _id = "34", Point = 10, KnockedBowling = "X", Turn = 1, Game_id = "123456" } };
            var scoreRepository = new Mock<IUnitOfWork>();
            var scoreService = new Mock<IScoreService>();
            scoreRepository.Setup(x => x.gameRepository<Score>().GetAllsAsync()).ReturnsAsync(scores.ToList());
            scoreService.Setup(x => x.GetTotalPoints("34")).ReturnsAsync(10);
        }
    }
}
