﻿
namespace Bowling.Application.Tests.Services
{
    using Bowling.Application.Interfaces;
    using Bowling.Domain.Entities;
    using Bowling.Domain.Interfaces;
    using Moq;
    using System.Collections.Generic;
    using System.Linq;
    using Xunit;

    public class GameServiceUnitTest
    {
        [Fact]
        public void Test_GetAllGamesAsync()
        {
            var games = new List<Game>() { new Game { _id = "343242323", GameName = "bowling", Status = "STARTED", PlayerName = "efrain", Difficulty = "EASY" } };
            var gameRepository = new Mock<IUnitOfWork>();
            var gameService = new Mock<IGameService>();
            gameRepository.Setup(x => x.gameRepository<Game>().GetAllsAsync()).ReturnsAsync(games.ToList());
            gameService.Setup(x=>x.GetGamesAsync()).ReturnsAsync(games.ToList());
        }

        [Fact]
        public void Test_CreateGameAsync()
        {
            Game data = new Game { _id = "343242323", GameName = "bowling", Status = "STARTED", PlayerName = "efrain", Difficulty = "EASY" };
            var gameRepository = new Mock<IUnitOfWork>();
            var gameService = new Mock<IGameService>();
            gameRepository.Setup(x => x.gameRepository<Game>().CreateAsync(data)).ReturnsAsync(data);
            gameService.Setup(x => x.CreateGameAsync(data)).ReturnsAsync(data);
        }
    }
}
