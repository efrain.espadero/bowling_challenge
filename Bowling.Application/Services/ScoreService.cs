﻿
namespace Bowling.Application.Services
{
    using Bowling.Application.Interfaces;
    using Bowling.Domain.Entities;
    using Bowling.Domain.Interfaces;
    public class ScoreService : IScoreService
    {
        private readonly IUnitOfWork _unitOfWork;
        public ScoreService(IUnitOfWork unitOfWork)
        {
            this._unitOfWork = unitOfWork;
        }
        public async Task<Score> CreateScoreAsync(Score model)
        {
            return await _unitOfWork.scoreRepository<Score>().CreateAsync(model);
        }

        public Task DeleteScoreByIdAsync(string id)
        {
            throw new NotImplementedException();
        }

        public async Task<List<Score>> GetScoresAsync()
        {
            return await _unitOfWork.scoreRepository<Score>().GetAllsAsync();
        }

        public Task<Score> SaveScoreAsync(Score model, string id)
        {
            throw new NotImplementedException();
        }

        #region Bussiness Logic customized
        public async Task<int> GetTotalPoints(string identifier)
        {
            int result=0;
            List<Score> scores = await _unitOfWork.scoreRepository<Score>().GetAllsAsync();
            scores.ForEach(item =>
            {
                if (item.Game_id == identifier)
                {
                    result = result + item.Point;
                }
            });
            return result;

        }
       #endregion
    }
}
