﻿
namespace Bowling.Application.Services
{
    using Bowling.Application.Interfaces;
    using Bowling.Domain.Entities;
    using Bowling.Domain.Interfaces;

    public class GameService : IGameService
    {
        private readonly IUnitOfWork _baseRepository;
        public GameService(IUnitOfWork baseRepository)
        {
            this._baseRepository = baseRepository;
        }
        public async Task<Game> CreateGameAsync(Game model)
        {
            return await _baseRepository.gameRepository<Game>().CreateAsync(model);
        }

        public Task DeleteGameByIdAsync(string id)
        {
            throw new NotImplementedException();
        }

        public Task<Game> GetGameByIdAsync(string id)
        {
            throw new NotImplementedException();
        }

        public async Task<List<Game>> GetGamesAsync()
        {
            return await this._baseRepository.gameRepository<Game>().GetAllsAsync();
        }

        public Task<Game> SaveGameAsync(Game model, string id)
        {
            throw new NotImplementedException();
        }

    }
}
