﻿using Bowling.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bowling.Application.Interfaces
{
    public interface IGameService
    {
        Task<List<Game>> GetGamesAsync();
        Task<Game> GetGameByIdAsync(string id);
        Task DeleteGameByIdAsync(string id);
        Task<Game> CreateGameAsync(Game model);
        Task<Game> SaveGameAsync(Game model, string id);
    }
}
