﻿using Bowling.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bowling.Application.Interfaces
{
    public interface IScoreService
    {
        Task<List<Score>> GetScoresAsync();
        Task DeleteScoreByIdAsync(string id);
        Task<Score> CreateScoreAsync(Score model);
        Task<Score> SaveScoreAsync(Score model, string id);
        Task<int> GetTotalPoints(string identifier);
    }
}
